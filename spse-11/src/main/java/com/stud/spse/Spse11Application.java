package com.stud.spse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spse11Application {

    public static void main(String[] args) {
        SpringApplication.run(Spse11Application.class, args);
    }

}
