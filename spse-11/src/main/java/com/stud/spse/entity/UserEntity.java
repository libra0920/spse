package com.stud.spse.entity;

import lombok.Data;

@Data
public class UserEntity {

    private String name;

    private String gender;

    private String phone;


    public UserEntity() {

    }

    public UserEntity(String name, String gender, String phone) {
        this.name = name;
        this.gender = gender;
        this.phone = phone;
    }

}
