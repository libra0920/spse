package com.stud.spse.controller;

import com.stud.spse.entity.UserEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ViewController {

    @RequestMapping("/user01")
    public String getIndex(Model model){
        List<UserEntity> userList = new ArrayList<>();
        UserEntity user = new UserEntity("tom","female", "17788996600");
        userList.add(user);
        model.addAttribute(userList);
        return "user01";
    }

}
