package com.stud.spse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spse05Application {

    public static void main(String[] args) {
        SpringApplication.run(Spse05Application.class, args);
    }

}
