package com.stud.spse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spse04Application {

    public static void main(String[] args) {
        SpringApplication.run(Spse04Application.class, args);
    }

}
