
#SpringSecurity学习实战
项目简称 Spse

## 第一章 最简单的SpringSecurity程序

## 第二章 认证 用户名密码登录

## 第三章 认证 图片验证码登录

## 第四章 认证 记住我

## 第五章 认证 手机验证码登录

## 第九章 会话管理

## 第十章 登出Logout

## 第十一章 OAuth2 授权码模式

## 第十二章 OAuth2 用户名密码登录获取TOKEN

## 第十五章 OAuth2 多客户端与JWT

普通token就是ID，数据需要存储在如Redis当中

JWT无需额外存储，可以直接根据TOKEN得出

反编译token：https://jwt.io/

