package com.stud.spse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OAuth2Client {

    private String clientId;
    private String clientSecret;
    private int accessTokenValiditySeconds;

}
