package com.stud.spse.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 手机验证码信息
 */
@Data
public class SmsCode {

    private String code;
    private LocalDateTime expireTime;


    public SmsCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public boolean isExpried() {
        return LocalDateTime.now().isAfter(expireTime);
    }

}
